const clientConfig = require('./app/config/webpack.client.config'),
    serverConfig = require('./app/config/webpack.server.config');

module.exports = [
    clientConfig,
    serverConfig,
];