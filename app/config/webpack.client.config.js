const path = require('path'),
    NODE_ENV = process.env.NODE_ENV,
    IS_DEV = NODE_ENV === 'development',
    templatesPath = path.resolve(__dirname, './app/templates/'),
    templateType = 'default',
    { CleanWebpackPlugin } = require('clean-webpack-plugin'),
    setupDevTool = () => IS_DEV ? 'eval' : false;

module.exports = {
    mode: NODE_ENV ? NODE_ENV : 'development',
    entry: path.resolve(__dirname, '../views/index.jsx'),
    output: {
        path: path.resolve(__dirname, '../../dist/client/'),
        filename: 'client.js',
    },
    resolve: {
        extensions: ['.jsx', '.js', '.json', '.tsx', '.ts'],
        alias: {
            'react-dom': IS_DEV ? '@hot-loader/react-dom' : 'react-dom',
        }
    },
    module: {
        rules: [
            {
                test: /\.[tj]sx?$/,
                use: ['ts-loader']
            },
            {
                test: /\.s[ac]ss?$/,
                use: ['style-loader', {
                    loader: 'css-loader',
                    options: {
                        modules: {
                            mode: 'local',
                            localIdentName: '[name]__[local]--[hash:base64:5]',
                        },
                    },
                }, 'sass-loader']
            },
            {
                include: path.resolve(__dirname, templatesPath + '/' + templateType + '/_template.pug'),
                use: ['raw-loader', {
                    loader: 'pug-html-loader',
                    options: {
                        pretty: true,
                    },
                }],
            },
            {
                test: /\.pug?$/,
                exclude: path.resolve(__dirname, templatesPath + '/' + templateType + '/_template.pug'),
                use: ['babel-loader', 'pug-as-jsx-loader']
            }
        ]
    },
    plugins: IS_DEV
    ? [
        new CleanWebpackPlugin(),
    ]
    : [],
    devtool: setupDevTool(),
};