const path = require('path'),
    NODE_ENV = process.env.NODE_ENV,
    IS_DEV = NODE_ENV === 'development',
    templatesPath = path.resolve(__dirname, './app/templates/'),
    templateType = 'default',
    nodeExternals = require('webpack-node-externals'),
    { CleanWebpackPlugin } = require('clean-webpack-plugin'),
    setupDevTool = () => IS_DEV ? 'eval' : false;

module.exports = {
    target: 'node',
    mode: NODE_ENV ? NODE_ENV : 'development',
    entry: path.resolve(__dirname, '../bin/server.js'),
    output: {
        path: path.resolve(__dirname, '../../dist/server/'),
        filename: 'server.js',
    },
    resolve: {
        extensions: ['.jsx', '.js', '.json', '.tsx', '.ts'],
    },
    externals: [nodeExternals()],
    module: {
        rules: [
            {
                test: /\.[tj]sx?$/,
                use: ['ts-loader']
            },
            {
                test: /\.pug?$/,
                use: ['pug-loader'],
            }
        ]
    },
    optimization: {
        minimize: !IS_DEV,
    }
}