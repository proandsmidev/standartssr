import React from 'react';
import ReactDOM from 'react-dom';

import header from '../components/header.pug';

window.addEventListener('load', () => {
    ReactDOM.hydrate(header.call(this, {}), document.querySelector('.header'));
});