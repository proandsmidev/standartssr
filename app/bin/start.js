const webpack = require('webpack'),
    [ webpackClientConfig, webpackServerConfig ] = require('../../webpack.config'),
    nodemon = require('nodemon'),
    path = require('path'),
    webpackDevMiddleware = require('webpack-dev-middleware'),
    webpackHotMiddleware = require('webpack-hot-middleware'),
    express = require('express'),
    hmrServer = express(),
    clientCompiler = webpack(webpackClientConfig),
    serverCompiler = webpack(webpackServerConfig);

hmrServer.use(webpackDevMiddleware(clientCompiler, {
    publicPath: webpackClientConfig.output.path,
    serverSideRender: true,
    noInfo: true,
    watchOptions: {
        ignore: /dist/,
    },
    writeToDisk: true,
    stats: 'errors-only'
}));

hmrServer.use(webpackHotMiddleware(clientCompiler, {
    path: '/static/__webpack_hmr',
}));

hmrServer.listen(3001, () => console.log('HMR SERVER STARTED'));

serverCompiler.run( err => {
    err
        ? console.error('Compilation failed: ', err)
        : console.log('Compilation was successfully');

    serverCompiler.watch({}, err => err ? console.error('Compilation failed: ', err) : console.log('Compilation was' +
        ' successfully'));

    nodemon({
        script: path.resolve(__dirname, '../../dist/server/server.js'),
        watch: [
            path.resolve(__dirname, '../../dist/server'),
            path.resolve(__dirname, '../../dist/client'),
        ],
        delay: 1000
    });
});