import express from 'express';
import ReactDOM from 'react-dom/server';

import header from '../components/header.pug';
import template from '../templates/default/_template.pug';

const app = express();

app.set('view engine', 'pug');

app.use('/static', express.static('./dist/client'));

app.get('/', (req, res) => {
    res.send(
        template.call(this, {
            header: header(),
        }),
    );
});

app.listen(3000, () => console.log('SERVER IS SUCCESSFUL STARTED ON http://localhost:3000'));

